# Overview

Build base image that will act as a base for building the LFS toolchain. This is the first step in building LFS.

Refer to the `docs` project for a general overview of the whole `automated-linux-from-scratch` group.

# Build

Build the base image with `docker`, e.g:

```shell script
$ docker build -t base-toolchain:dev - < Dockerfile
```

The build attempts to closely follow 
[LFS host requirements](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter02/hostreqs.html) recommendations. 
You should only experiment with higher versions of packages once you complete the whole chain once.
 