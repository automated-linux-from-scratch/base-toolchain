FROM ubuntu:focal-20201106
LABEL maintainer="Aleksander Ciołek <aleksander.ciolek@protonmail.com>"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y \
       ansible \
       bison \
       build-essential \
       gawk \
       sudo \
       texinfo \
    && apt-get clean \
    && ln -sf /bin/bash /bin/sh \
    && useradd --create-home --no-log-init lfs

CMD ["/bin/bash"]
